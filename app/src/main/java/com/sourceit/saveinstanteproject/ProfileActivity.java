package com.sourceit.saveinstanteproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile)
    TextView profile;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        if (getIntent() != null
                && getIntent().getParcelableExtra(MainActivity.KEY_LABEL) != null) {
            user = getIntent().getParcelableExtra(MainActivity.KEY_LABEL);
            profile.setText(String.format("Hello, %s. Your email is %s.", user.name, user.email));
        }
    }
}
