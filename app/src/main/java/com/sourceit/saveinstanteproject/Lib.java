package com.sourceit.saveinstanteproject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wenceslaus on 08.05.18.
 */

public class Lib implements Parcelable{
    public String value;

    protected Lib(Parcel in) {
        value = in.readString();
    }

    public static final Creator<Lib> CREATOR = new Creator<Lib>() {
        @Override
        public Lib createFromParcel(Parcel in) {
            return new Lib(in);
        }

        @Override
        public Lib[] newArray(int size) {
            return new Lib[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }
}
