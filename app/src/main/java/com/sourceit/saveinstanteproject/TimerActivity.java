package com.sourceit.saveinstanteproject;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimerActivity extends AppCompatActivity {

    @BindView(R.id.timer)
    TextView timer;

    int count = 5;


    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            count--;
            if (count > 0) {
                timer.setText(String.valueOf(count));
                handler.sendEmptyMessageDelayed(1, 1000);
            } else {
                timer.setText("end");
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        ButterKnife.bind(this);

        timer.setText(String.valueOf(count));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (count > 0) {
            handler.sendEmptyMessageDelayed(1, 1000);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeMessages(1);
    }
}
