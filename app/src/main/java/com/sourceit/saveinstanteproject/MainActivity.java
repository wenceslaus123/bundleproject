package com.sourceit.saveinstanteproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_LABEL = "key_label";

    @BindView(R.id.label)
    TextView label;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            user = (User) savedInstanceState.getParcelable(KEY_LABEL);
            label.setText(user.name + " " + user.email);
        }
    }

    @OnClick(R.id.update)
    public void onClick() {
        user = new User(name.getText().toString(), email.getText().toString());
        label.setText(user.name + " " + user.email);


        Intent intent = new Intent(this, TimerActivity.class);
//        intent.putExtra(KEY_LABEL, user);
        startActivity(intent);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_LABEL, user);
    }
}
